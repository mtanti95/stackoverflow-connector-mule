package org.mule.modules.stackoverflow2.client;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class requesterClient {
	public Map<String, Object>  makeRequest(String URL) throws IOException{
		ObjectMapper mapper = new ObjectMapper();
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(URL);
		request.addHeader("User-Agent", "");
		request.addHeader("Content-Encoding", "gzip");
		HttpResponse response = client.execute(request);
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		
		StringBuilder result = new StringBuilder();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		map = mapper.readValue(result.toString(), new TypeReference<Map<String, Object>>(){});
		rd.close();
		return map;
	}
}
