package org.mule.modules.stackoverflow2.config;

import org.mule.api.annotations.components.Configuration;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.http.HttpEntity;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.mule.api.annotations.Configurable;
import org.mule.api.annotations.param.Default;
import org.mule.api.annotations.param.Optional;
import org.mule.modules.stackoverflow2.client.requesterClient;


@Configuration(friendlyName = "Configuration")
public class ConnectorConfig {

	@Configurable
    @Default("https://api.stackexchange.com/2.2/questions")
    String URL; 
	
	@Configurable
	@Optional
	@Default("asc")
	String order;
	
	@Configurable
	@Optional
	@Default("creation")
	String sort;
	
	@Configurable
	@Optional
	@Default("stackoverflow")
	String site;
	
	@Configurable
	@Optional
	@Default("ANIiuLyXnGi6cY4WKhVQiA((")
	String key;
	
	@Configurable
	@Default("football,animals,cars")
	String queryOptions;
	
	int page = 1;
	
	long fromdate = 0;
    
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public Long getFromdate() {
		return fromdate;
	}

	public void setFromdate(Long fromdate) {
		this.fromdate = fromdate;
	}

	private requesterClient client = new requesterClient();
    
	
	
    public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}
	
	public requesterClient getClient() {
		return client;
	}

	public void setClient(requesterClient client) {
		this.client = client;
	}
	
	public String [] queryOpts (){
		String [] opts = queryOptions.split(",");
		return null;
	}
	
	public String request() {
		URIBuilder builder = new URIBuilder();
	 
	    if (order != null){
	       builder.addParameter("order", order);
	    }
	    if (sort != null){
	    	builder.addParameter("sort", sort);
	    }
	    if(site!=null){
	    	builder.addParameter("site", site);
	    }
	    if(key!=null){
	    	builder.addParameter("key", key);
	    }
	    builder.addParameter("page", Integer.toString(page));
	    builder.addParameter("fromdate", Long.toString(fromdate));

	    return URL + builder.toString();
	}
}
