package org.mule.modules.stackoverflow2;


import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.mule.api.annotations.Config;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Source;
import org.mule.api.annotations.SourceStrategy;
import org.mule.api.callback.SourceCallback;
import org.mule.modules.stackoverflow2.client.requesterClient;
import org.mule.modules.stackoverflow2.config.ConnectorConfig;

@Connector(name="stackoverflow", friendlyName="Stackoverflow")
public class Stackoverflow2Connector {

    @Config
    ConnectorConfig config;
    requesterClient client;

    @Source(sourceStrategy=SourceStrategy.POLLING, pollingPeriod=3600000)
    public void getData(SourceCallback callback) throws Exception {
    	Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -60);
		Date date = calendar.getTime();
		long unixTime = date.getTime()/1000;
    	
    	int page = 1;
    	while(true){
    		Thread.sleep(5000);
    		
    		config.setPage(page);
    		config.setFromdate(unixTime);
    		
    		String url = config.request();
    		
    		System.out.println("Request:" + url);
    		Map<String, Object> map = client.makeRequest(url);
    		callback.process(map);
    		
    		if(map.get("has_more").equals(true)){
    			page++;
    		}else{
    			break;
    		}
    	}
    }

    public ConnectorConfig getConfig() {
        return config;
    }

    public void setConfig(ConnectorConfig config) {
        this.config = config;
        this.client = config.getClient();
    }
}